# Define specific targets
.PHONY: clean, lib, lib-s, _buildLib-s, _buildLib-d, lib-d, static

# Remove implicit rules
.SUFFIXES:

# Variables
CC = g++
CFLAGS = -W -Wall -std=c++14
TARGET = foo
LIB = -L. -lfoo
LIB-D-TARGET = libfoo.so

# Build for the program
all: foo.o lib-d
	$(CC) foo.o -o $(TARGET) $(CFLAGS) $(LIB)

static: foo.o lib-s
	$(CC) -static foo.o $(LIB) -o $(TARGET) $(CFLAGS)

foo.o: foo.cpp fooLib.hpp
	$(CC) -c foo.cpp -o foo.o $(CFLAGS)

# Alias: "lib" calls "lib-d"
lib: lib-d

# Build for dynamic library
lib-d: _buildLib-d
	$(CC) -shared -o $(LIB-D-TARGET) fooLib.o

_buildLib-d: fooLib.cpp
	$(CC) -fPIC -c fooLib.cpp -o fooLib.o

# Build for static library
lib-s: _buildLib-s
	ar rcs libfoo.a fooLib.o

_buildLib-s: fooLib.cpp
	$(CC) -c fooLib.cpp -o fooLib.o $(CFLAGS)

# clean
clean:
	rm -rf *.o
