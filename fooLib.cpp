#include "fooLib.hpp"

void foo()
{
    std::cout << "You called foo function!" << std::endl;
}

void foo(const std::string &pMessage)
{
    std::cout << "You called foo function with one parameter: " << pMessage << std::endl;
}
