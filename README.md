# Introduction
This little project is a test for static and dynamic compilation in C++.
The project contains a small library which provides 2 functions that print text into console, and a main program that call the library.

# Compilation
## Dynamic
Library only:
> make lib-d

Program (with library):
> make

## Static
Library only:
> make lib-s

Program:
> make static

## Clean object files:
> make clean

